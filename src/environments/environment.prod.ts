export const environment = {
  production: true,
  socketEndpoint: 'https://fathomless-scrubland-58077.herokuapp.com'
};
