import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BaseFormFieldComponent } from 'src/app/shared/abstracts/base-form-field.component';

export enum InputType {
  text = 'text',
  email = 'email',
  password = 'password',
  number = 'number',
  date = 'date'
}

export enum TooltipPosition {
  left = 'left',
  right = 'right',
  above = 'above',
  below = 'below',
  before = 'before',
  after = 'after'
}
@Component({
  selector: 'base-form-input',
  templateUrl: './base-input.component.html',
  styleUrls: ['./base-input.component.scss']
})
export class BaseInputComponent extends BaseFormFieldComponent {
  @Input() iconSuffix: string;
  @Input() label: string;
  @Input() validationDisplayed = true;
  @Input() isButtonShown = false;
  @Input() buttonIcon: string;
  @Input() type = InputType.text;
  @Input() isButtonDisabled = false;
  @Input() inputTooltipText: string;
  @Input() inputTooltipPosition: TooltipPosition = TooltipPosition.above;
  @Input() buttonTooltipText: string;
  @Input() buttonTooltipPosition: TooltipPosition = TooltipPosition.above;
  @Output() buttonClicked = new EventEmitter<void>();

  onButtonClicked(): void {
    this.buttonClicked.emit();
  }
}
