import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { BaseInputComponent, InputType } from './base-input.component';
import { MatTooltipHarness } from '@angular/material/tooltip/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatInputHarness } from '@angular/material/input/testing';
import { click } from 'src/app/modules/utils/test-utils';

describe('BaseFormInputComponent', () => {
  let component: BaseInputComponent;
  let fixture: ComponentFixture<BaseInputComponent>;
  let el: DebugElement;
  let loader: HarnessLoader;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [MaterialModule, ReactiveFormsModule, FormsModule, NoopAnimationsModule],
        declarations: [BaseInputComponent]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(BaseInputComponent);
          component = fixture.componentInstance;
          el = fixture.debugElement;
          loader = TestbedHarnessEnvironment.loader(fixture);
        });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load input with a control value', async () => {
    component.control = new FormControl('Test value');
    fixture.detectChanges();

    const inputs = await loader.getAllHarnesses(MatInputHarness.with({ value: 'Test value' }));
    expect(inputs.length).toBe(1);
  });

  it('should have input type which corresponds to the type provided', async () => {
    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(1);

    component.type = InputType.password;
    fixture.detectChanges();

    expect(await inputs[0].getType()).toBe(InputType.password);

    component.type = InputType.email;
    fixture.detectChanges();

    expect(await inputs[0].getType()).toBe(InputType.email);
  });

  it('should have required input when required is true', async () => {
    component.required = true;
    fixture.detectChanges();

    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(1);

    expect(await inputs[0].isRequired()).toBe(true);
  });

  it('should not have required input when required is false (default)', async () => {
    fixture.detectChanges();

    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(1);

    expect(await inputs[0].isRequired()).toBe(false);
  });

  it('should have readonly input when readonly is true', async () => {
    component.readonly = true;
    fixture.detectChanges();

    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(1);

    expect(await inputs[0].isReadonly()).toBe(true);
  });

  it('should not have readonly input when readonly is false (default)', async () => {
    fixture.detectChanges();

    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(1);

    expect(await inputs[0].isReadonly()).toBe(false);
  });

  it('should have prefix icon when icon is passed', () => {
    component.icon = 'radar';
    fixture.detectChanges();

    const icon = el.query(By.css('.mat-form-field-prefix .mat-icon[matprefix]'));
    const iconText = icon.nativeElement.textContent;

    expect(icon).toBeTruthy();
    expect(iconText).toEqual('radar');
  });

  it('should not have prefix icon when icon is not passed (default)', () => {
    fixture.detectChanges();

    const icon = el.query(By.css('.mat-form-field-prefix .mat-icon[matprefix]'));

    expect(icon).toBeFalsy();
  });

  it('should have suffix icon when iconSuffix is passed', () => {
    component.iconSuffix = 'radar';
    fixture.detectChanges();

    const suffixIcon = el.query(By.css('.mat-form-field-suffix .mat-icon[matsuffix]'));
    const suffixIconText = suffixIcon.nativeElement.textContent;

    expect(suffixIcon).toBeTruthy();
    expect(suffixIconText).toEqual('radar');
  });

  it('should not have suffix icon when iconSuffix is not passed (default)', () => {
    fixture.detectChanges();

    const icon = el.query(By.css('.mat-form-field-suffix .mat-icon[matprefix]'));

    expect(icon).toBeFalsy();
  });

  it('should have button when both isButtonShown and buttonIcon are passed', () => {
    component.buttonIcon = 'radar';
    component.isButtonShown = true;
    fixture.detectChanges();

    const button = el.query(By.css('.mat-form-field-suffix button .mat-icon'));
    const buttonText = button.nativeElement.textContent;

    expect(button).toBeTruthy();
    expect(buttonText).toEqual('radar');
  });

  it('should not have button when buttonIcon is passed, but isButtonShown is not passed', () => {
    component.buttonIcon = 'radar';
    fixture.detectChanges();

    const button = el.query(By.css('.mat-form-field-suffix button'));

    expect(button).toBeFalsy();
  });

  it('should not have button when isButtonShown is passed, but buttonIcon is not passed', () => {
    component.isButtonShown = true;
    fixture.detectChanges();

    const button = el.query(By.css('.mat-form-field-suffix button'));

    expect(button).toBeFalsy();
  });

  it('should have button in disabled state when isButtonDisabled is true', () => {
    component.buttonIcon = 'radar';
    component.isButtonShown = true;
    component.isButtonDisabled = true;
    fixture.detectChanges();

    const buttonWithDisabledClass = el.query(By.css('.mat-form-field-suffix button.mat-button-disabled'));

    expect(buttonWithDisabledClass).toBeTruthy();
  });

  it('should have label when label is passed', () => {
    component.label = 'Input label';
    fixture.detectChanges();

    const label = el.query(By.css('.mat-form-field-infix .mat-form-field-label'));
    const labelText = label.nativeElement.textContent;

    expect(label).toBeTruthy();
    expect(labelText).toEqual('Input label');
  });

  it('should not have label when label is not passed (default)', () => {
    fixture.detectChanges();

    const label = el.query(By.css('.mat-form-field-infix .mat-form-field-label'));

    expect(label).toBeFalsy();
  });

  it('should be able to get the text of an input tooltip', async () => {
    component.inputTooltipText = 'Input tooltip';
    fixture.detectChanges();

    const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '#input' }));
    await tooltip.show();
    expect(await tooltip.getTooltipText()).toBe('Input tooltip');
  });

  it('should be able to get the text of a button tooltip', async () => {
    component.isButtonShown = true;
    component.buttonIcon = 'radar';
    component.buttonTooltipText = 'Button tooltip';
    fixture.detectChanges();

    const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '#inputButton' }));
    await tooltip.show();
    expect(await tooltip.getTooltipText()).toBe('Button tooltip');
  });

  it('should trigger onButtonClicked method when input button is clicked', fakeAsync(() => {
    const spy = spyOn(component, 'onButtonClicked');

    component.isButtonShown = true;
    component.buttonIcon = 'radar';
    fixture.detectChanges();

    const button = el.query(By.css('.mat-form-field-suffix button'));
    click(button);
    fixture.detectChanges();
    flush();

    expect(spy).toHaveBeenCalledTimes(1);
  }));
});
