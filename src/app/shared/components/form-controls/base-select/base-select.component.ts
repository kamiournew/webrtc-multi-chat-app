import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { BaseFormFieldComponent } from 'src/app/shared/abstracts/base-form-field.component';

export interface SelectOption {
  value: any;
  label: string;
}

@Component({
  selector: 'base-select',
  templateUrl: './base-select.component.html',
  styleUrls: ['./base-select.component.scss']
})
export class BaseSelectComponent extends BaseFormFieldComponent {
  @Input() options: SelectOption[];
  @Input() multiple = false;
  @Output() valueSelected: EventEmitter<MatSelectChange> = new EventEmitter<MatSelectChange>(null);

  onSelectionChange(event: MatSelectChange): void {
    this.valueSelected.emit(event);
  }
}
