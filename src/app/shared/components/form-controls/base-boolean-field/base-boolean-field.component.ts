import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BaseFormFieldComponent } from 'src/app/shared/abstracts/base-form-field.component';

export enum BooleanFieldType {
  checkbox = 'checkbox',
  toggle = 'toggle'
}

@Component({
  selector: 'base-boolean-field',
  templateUrl: './base-boolean-field.component.html',
  styleUrls: ['./base-boolean-field.component.scss']
})
export class BaseBooleanFieldComponent extends BaseFormFieldComponent {
  readonly BooleanFieldType: typeof BooleanFieldType = BooleanFieldType;
  @Input() type = BooleanFieldType.checkbox;
  @Input() label: string;
  @Input() stopPropagation: boolean;
  @Output() controlChange: EventEmitter<string> = new EventEmitter();

  onCheckboxInput(event: Event): void {
    if (this.stopPropagation) {
      event.stopPropagation();
    }
    if (event.type === 'input') {
      this.controlChange.emit('');
    }
  }
}
