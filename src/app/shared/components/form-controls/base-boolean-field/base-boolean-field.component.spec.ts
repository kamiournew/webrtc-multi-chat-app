import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { MatTooltipHarness } from '@angular/material/tooltip/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { BaseBooleanFieldComponent, BooleanFieldType } from './base-boolean-field.component';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatSlideToggleHarness } from '@angular/material/slide-toggle/testing';
import { click } from 'src/app/modules/utils/test-utils';

describe('BaseBooleanFieldComponent', () => {
  let component: BaseBooleanFieldComponent;
  let fixture: ComponentFixture<BaseBooleanFieldComponent>;
  let el: DebugElement;
  let loader: HarnessLoader;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [MaterialModule, ReactiveFormsModule, FormsModule, NoopAnimationsModule],
        declarations: [BaseBooleanFieldComponent]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(BaseBooleanFieldComponent);
          component = fixture.componentInstance;
          el = fixture.debugElement;
          loader = TestbedHarnessEnvironment.loader(fixture);
        });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load checkbox field with a passed true value', async () => {
    component.type = BooleanFieldType.checkbox;
    component.control = new FormControl(true);
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness);
    expect(checkboxes.length).toBe(1);
    expect(await checkboxes[0].isChecked()).toBe(true);
  });

  it('should load toggle field with a passed true value', async () => {
    component.type = BooleanFieldType.toggle;
    component.control = new FormControl(true);
    fixture.detectChanges();

    const toggles = await loader.getAllHarnesses(MatSlideToggleHarness);
    expect(toggles.length).toBe(1);
    expect(await toggles[0].isChecked()).toBe(true);
  });

  it('should be checkbox if "checkbox" type is passed', async () => {
    component.type = BooleanFieldType.checkbox;
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness);
    const toggles = await loader.getAllHarnesses(MatSlideToggleHarness);

    expect(checkboxes.length).toBe(1);
    expect(toggles.length).toBe(0, 'Should be no toggles');
  });

  it('should be toggle if "toggle" type is passed', async () => {
    component.type = BooleanFieldType.toggle;
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness);
    const toggles = await loader.getAllHarnesses(MatSlideToggleHarness);

    expect(toggles.length).toBe(1);
    expect(checkboxes.length).toBe(0, 'Should be no checkboxes');
  });

  it('should have required checkbox when required is true and type is "checkbox"', async () => {
    component.type = BooleanFieldType.checkbox;
    component.required = true;
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness);
    expect(checkboxes.length).toBe(1);

    expect(await checkboxes[0].isRequired()).toBe(true);
  });

  it('should have required toggle when required is true  and type is "toggle"', async () => {
    component.type = BooleanFieldType.toggle;
    component.required = true;
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatSlideToggleHarness);
    expect(checkboxes.length).toBe(1);

    expect(await checkboxes[0].isRequired()).toBe(true);
  });

  it('should have disabled checkbox when disabled is true  and type is "checkbox"', async () => {
    component.type = BooleanFieldType.checkbox;
    component.disabled = true;
    fixture.detectChanges();

    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness);
    expect(checkboxes.length).toBe(1);

    expect(await checkboxes[0].isDisabled()).toBe(true);
  });

  it('should have disabled toggle when disabled is true  and type is "toggle"', async () => {
    component.type = BooleanFieldType.toggle;
    component.disabled = true;
    fixture.detectChanges();

    const toggles = await loader.getAllHarnesses(MatSlideToggleHarness);
    expect(toggles.length).toBe(1);

    expect(await toggles[0].isDisabled()).toBe(true);
  });

  it('should have checkbox label when label is passed and type is "checkbox"', async () => {
    component.type = BooleanFieldType.checkbox;
    component.label = 'Checkbox label';
    fixture.detectChanges();

    const checkbox = await loader.getHarness(MatCheckboxHarness.with({ label: 'Checkbox label' }));
    expect(checkbox).toBeTruthy();
  });

  it('should emit empty string when checkbox label is clicked and stopPropagation is true', fakeAsync(() => {
    const spy = spyOn(component.controlChange, 'emit');

    component.type = BooleanFieldType.checkbox;
    component.stopPropagation = true;
    fixture.detectChanges();

    const checkboxLabel = el.query(By.css('.mat-checkbox label'));
    click(checkboxLabel.nativeElement);
    fixture.detectChanges();
    flush();

    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith('');
  }));
});
