import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { ChangeDetectorRef, Component, Input, NgZone, ViewChild } from '@angular/core';
import { take } from 'rxjs/operators';
import { BaseFormFieldComponent } from 'src/app/shared/abstracts/base-form-field.component';

@Component({
  selector: 'base-form-textarea',
  templateUrl: './base-textarea.component.html',
  styleUrls: ['./base-textarea.component.scss']
})
export class BaseTextareaComponent extends BaseFormFieldComponent {
  @Input() label: string;
  @Input() validationDisplayed = true;
  @Input() minRows = 3;
  @Input() maxRows = 6;
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  constructor(private ngZone: NgZone, cdr: ChangeDetectorRef) {
    super(cdr);
  }

  triggerResize(): void {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
  }
}
