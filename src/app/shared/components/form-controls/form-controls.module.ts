import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { BaseSelectComponent } from './base-select/base-select.component';
import { BaseInputComponent } from './base-input/base-input.component';
import { BaseBooleanFieldComponent } from './base-boolean-field/base-boolean-field.component';
import { BaseTextareaComponent } from './base-textarea/base-textarea.component';

@NgModule({
  declarations: [BaseSelectComponent, BaseInputComponent, BaseBooleanFieldComponent, BaseTextareaComponent],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    BaseSelectComponent,
    BaseInputComponent,
    BaseBooleanFieldComponent,
    BaseTextareaComponent
  ],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule]
})
export class AppFormControlsModule {}
