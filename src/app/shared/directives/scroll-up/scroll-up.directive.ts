import { Directive, ElementRef, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appScrollUp]'
})
export class ScrollUpDirective implements OnInit, OnDestroy {
  @Input() appScrollUp: Subject<void>;
  private destroyed$: Subject<void> = new Subject<void>();

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {
    this.appScrollUp.pipe(takeUntil(this.destroyed$), delay(0)).subscribe(() => {
      this.renderer.setProperty(this.el.nativeElement, 'scrollTop', this.el.nativeElement.scrollHeight);
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
