import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  template: ''
})
export abstract class BaseFormFieldComponent implements OnChanges, OnDestroy {
  private destroy: Subject<void> = new Subject<void>();
  @Input() icon: string;
  @Input() placeholder: string;
  @Input() required = false;
  @Input() disabled = false;
  @Input() readonly = false;
  @Input() appearance: 'legacy' | 'standard' | 'fill' | 'outline' = 'outline';
  @Input() value: any = null;
  @Input() control: AbstractControl = new FormControl();

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnChanges({ value, disabled, placeholder, control }: SimpleChanges): void {
    if (disabled && typeof disabled.currentValue === 'boolean') {
      if (this.disabled) {
        this.control.disable();
      } else {
        this.control.enable();
      }
    }

    if (control?.currentValue instanceof AbstractControl) {
      control.currentValue.valueChanges.pipe(takeUntil(this.destroy)).subscribe(() => this.cdr.detectChanges());
    }

    if (value) {
      this.control.setValue(value.currentValue);
    }

    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  get formControl(): FormControl {
    return this.control as FormControl;
  }

  get errorMessage(): string {
    switch (true) {
      case this.control.hasError('mustMatch'):
        return 'Must match';
      case this.control.hasError('required'):
        return 'Required field';
      case this.control.hasError('email'):
        return 'Invalid email';
      case this.control.hasError('password'):
        return 'Password is not strong enough';
      case this.control.hasError('digitsOnly'):
        return 'Must contain digits only';
      case this.control.hasError('digitsAndLetters'):
        return 'Must contain digits and letters';
      case this.control.hasError('minlength'):
        return `Minimum length is ${this.control.getError('minlength').requiredLength}`;
      case this.control.hasError('maxlength'):
        return `Maximum length is ${this.control.getError('maxlength').requiredLength}`;
      case this.control.hasError('maxsize'):
        return `Maximum size is ${this.control.getError('maxsize').requiredSize}`;
      default:
        return null;
    }
  }
}
