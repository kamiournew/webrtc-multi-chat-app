import { Directive, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup, NgForm } from '@angular/forms';

export interface FormControls {
  [key: string]: AbstractControl;
}

@Directive()
export abstract class BaseForm {
  formGroup: FormGroup;
  @ViewChild('ngFormRef') ngFormRef: NgForm;

  get formControls(): FormControls {
    return this.formGroup.controls;
  }

  clearForm(): void {
    this.ngFormRef.resetForm();
  }
}
