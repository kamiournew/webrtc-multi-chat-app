import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material/material.module';
import { NotFoundPageModule } from './components/not-found-page/not-found-page.module';
import { ScrollUpDirective } from './directives/scroll-up/scroll-up.directive';
import { ClipboardModule } from '@angular/cdk/clipboard';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NotFoundPageModule,
    ClipboardModule
  ],
  exports: [MaterialModule, ReactiveFormsModule, FormsModule, RouterModule, ScrollUpDirective, ClipboardModule],
  declarations: [ScrollUpDirective]
})
export class SharedModule {}
