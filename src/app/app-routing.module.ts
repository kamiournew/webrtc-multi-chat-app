import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainModule } from './modules/main/main.module';

const routes: Routes = [
  // {
  //   path: 'auth',
  // canActivate: [UnauthGuard],
  // component: AuthLayoutComponent,
  //   loadChildren: (): Promise<AuthModule> =>
  //     import('@client/views/auth/auth.module').then((m: { AuthModule: AuthModule }): AuthModule => m.AuthModule)
  // },
  {
    path: '',
    // canActivate: [AuthGuard],
    // resolve: { me: MeResolver },
    runGuardsAndResolvers: 'always',
    loadChildren: (): Promise<MainModule> =>
      import('./modules/main/main.module').then((m: { MainModule: MainModule }): MainModule => m.MainModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
