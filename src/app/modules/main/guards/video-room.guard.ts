import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SocketService } from '../services/socket-service/socket.service';

@Injectable({
  providedIn: 'root'
})
export class VideoRoomGuard implements CanActivate {
  constructor(private router: Router, private socketService: SocketService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const canEnterRoom = this.socketService.isRoomCreated;

    if (!canEnterRoom) {
      this.router.navigate(['', 'home'], {
        queryParams: {
          ['roomToken']: route.params.roomToken
        }
      });
      return false;
    }

    return true;
  }
}
