import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/shared/abstracts/base-form';
import { BooleanFieldType } from 'src/app/shared/components/form-controls/base-boolean-field/base-boolean-field.component';
import { InputType } from 'src/app/shared/components/form-controls/base-input/base-input.component';
import { SocketService } from '../../services/socket-service/socket.service';

@Component({
  selector: 'app-join-room-form',
  templateUrl: './join-room-form.component.html',
  styleUrls: ['./join-room-form.component.scss']
})
export class JoinRoomFormComponent extends BaseForm implements OnInit, AfterViewInit {
  InputType: typeof InputType = InputType;
  BooleanFieldType: typeof BooleanFieldType = BooleanFieldType;
  @ViewChild('userNameControl', { read: ElementRef, static: false }) userNameControl: ElementRef;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly socketService: SocketService,
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    const providedRoomToken = this.route.snapshot.queryParams.roomToken;

    this.formGroup = this.formBuilder.group({
      roomToken: new FormControl(providedRoomToken ?? '', [Validators.required, Validators.maxLength(20)]),
      userName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      muteAudio: new FormControl(false, [Validators.required, Validators.maxLength(50)]),
      hideVideo: new FormControl(false, [Validators.required, Validators.maxLength(50)])
    });
  }

  ngAfterViewInit(): void {
    this.userNameControl.nativeElement.querySelector('input').focus();
    this.cdr.detectChanges(); // to handle ExpressionChangedAfterItHasBeenCheckedError error
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.socketService.onJoinRoomButtonClicked(this.formGroup.value);
  }

  onClear(): void {
    this.clearForm();
    this.formControls.muteAudio.setValue(false);
    this.formControls.hideVideo.setValue(false);
  }
}
