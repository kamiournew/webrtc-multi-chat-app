import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/shared/abstracts/base-form';
import { InputType } from 'src/app/shared/components/form-controls/base-input/base-input.component';
import { SelectOption } from 'src/app/shared/components/form-controls/base-select/base-select.component';
import { SocketService } from '../../services/socket-service/socket.service';
import * as RandomWords from 'random-words';

@Component({
  selector: 'app-create-room-form',
  templateUrl: './create-room-form.component.html',
  styleUrls: ['./create-room-form.component.scss']
})
export class CreateRoomFormComponent extends BaseForm implements OnInit, AfterViewInit {
  InputType: typeof InputType = InputType;
  numberOfParticipantsValues: SelectOption[] = [
    {
      value: 2,
      label: 'Two'
    },
    {
      value: 3,
      label: 'Three'
    },
    {
      value: 4,
      label: 'Four'
    }
  ];
  @ViewChild('userNameControl', { read: ElementRef, static: false }) userNameControl: ElementRef;

  constructor(private formBuilder: FormBuilder, private socketService: SocketService, private cdr: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      roomName: new FormControl(this.getRandomRoomName(), [Validators.required, Validators.maxLength(50)]),
      userName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      maxNumber: new FormControl(3, [Validators.required])
    });
  }

  ngAfterViewInit(): void {
    this.userNameControl.nativeElement.querySelector('input').focus();
    this.cdr.detectChanges(); // to handle ExpressionChangedAfterItHasBeenCheckedError error
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.socketService.onCreateRoomButtonClicked(this.formGroup.value);
  }

  onClear(): void {
    this.clearForm();
  }

  onGenerateRoomName(): void {
    this.formControls.roomName.setValue(this.getRandomRoomName());
  }

  private getRandomRoomName(): string {
    const [name] = RandomWords({
      exactly: 1,
      wordsPerString: 2,
      separator: '-'
    });

    return `${name}-room`;
  }
}
