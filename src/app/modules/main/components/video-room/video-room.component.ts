import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SocketService } from '../../services/socket-service/socket.service';
import { Clipboard } from '@angular/cdk/clipboard';
import { PeerVideoConfig, UserMediaService } from '../../services/user-media-service/user-media.service';

@Component({
  selector: 'app-video-room',
  templateUrl: './video-room.component.html',
  styleUrls: ['./video-room.component.scss']
})
export class VideoRoomComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('userVideo') userVideo: ElementRef;
  @ViewChild('videoContainer', { read: ViewContainerRef }) videoContainer: ViewContainerRef;
  @ViewChildren('peerVideo') peerVideos: QueryList<ElementRef>;
  @Output() toggleDrawer = new EventEmitter<void>();
  private destroyed$: Subject<void> = new Subject<void>();
  private isInCreateMode: boolean;
  isMuted = false;
  isVideoHidden = false;
  roomName: string;
  creatorName: string;
  myName: string;

  get peerVideosCollection(): Array<PeerVideoConfig> {
    return this.userMediaService.peerVideosCollection;
  }

  get numberOfVideos(): number {
    return this.peerVideosCollection.length + 1;
  }

  get videoContainerGridRows(): string {
    return this.numberOfVideos > 2 ? '1fr 1fr' : '1fr';
  }

  get videoContainerGridColumns(): string {
    return this.numberOfVideos > 1 ? '1fr 1fr' : '1fr';
  }

  get videoContainerWidth(): string {
    return this.numberOfVideos !== 2 ? 'max-content' : '100%';
  }

  get videoWidth(): string {
    return this.numberOfVideos === 2 ? '100%' : 'auto';
  }

  get videoHeight(): string {
    return this.numberOfVideos === 2 ? 'auto' : '100%';
  }

  constructor(
    private readonly socketService: SocketService,
    private readonly userMediaService: UserMediaService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly toastr: ToastrService,
    private clipboard: Clipboard
  ) {}

  ngOnInit(): void {
    this.isInCreateMode = this.route.snapshot.queryParams.isCreate;
    this.isMuted = this.userMediaService.muteAudioFlag;
    this.isVideoHidden = this.userMediaService.hideVideoFlag;
    this.roomName = this.socketService.roomName;
    this.creatorName = this.socketService.creatorName;
    this.myName = this.socketService.userName;
  }

  onVideoLoaded(event: Event): void {
    (event.target as HTMLVideoElement).play();
  }

  ngAfterViewInit(): void {
    this.userVideo.nativeElement.muted = true;
    const mediaStream = this.userMediaService.userStream;

    this.userVideo.nativeElement.srcObject = mediaStream;
    this.userVideo.nativeElement.onloadedmetadata = () => {
      this.userVideo.nativeElement.play();
    };

    if (this.isInCreateMode) {
      this.socketService.emitReady();
    }
  }

  leaveRoom(): void {
    this.router.navigate(['', 'home']);
  }

  copyLink(): void {
    this.clipboard.copy(this.socketService.linkToJoinRoom);
    this.toastr.success('', 'Link copied');
  }

  onMuteAudio(): void {
    this.isMuted = !this.isMuted;
    this.socketService.onMuteAudioButtonClicked(this.isMuted);
  }

  onHideVideo(): void {
    this.isVideoHidden = !this.isVideoHidden;
    this.socketService.onHideVideoButtonClicked(this.isVideoHidden);
  }

  onToggleChat(): void {
    this.toggleDrawer.emit();
  }

  doCleanup(): void {
    this.destroyed$.next();
    this.destroyed$.complete();

    if (this.userVideo.nativeElement.srcObject) {
      /* stopping our audio and video streams */
      this.userVideo.nativeElement.srcObject.getTracks().forEach((track: MediaStreamTrack) => track.stop());
    }

    this.socketService.onLeaveRoomButtonClicked();
  }

  @HostListener('window:beforeunload')
  ngOnDestroy(): void {
    this.doCleanup();
  }
}
