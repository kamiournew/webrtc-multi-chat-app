import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { BaseForm } from 'src/app/shared/abstracts/base-form';
import { ChatService } from '../../../services/chat-service/chat.service';
import { SocketService } from '../../../services/socket-service/socket.service';

export interface Message {
  text: string;
  isMine?: boolean;
  authorName: string;
  authorId: string;
  time: Date;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent extends BaseForm implements OnInit {
  messages: Array<Message> = [];
  messageReceived: Subject<void>;

  constructor(
    private formBuilder: FormBuilder,
    private socketService: SocketService,
    private chatService: ChatService
  ) {
    super();
  }

  ngOnInit(): void {
    this.messages = this.chatService.messages;
    this.messageReceived = this.chatService.messageReceived;

    this.formGroup = this.formBuilder.group({
      messageText: new FormControl('', [Validators.maxLength(500)])
    });
  }

  onSubmit(): false {
    if (this.formGroup.invalid) {
      return;
    }

    if (!this.formControls.messageText.value.trim()) {
      return;
    }

    this.socketService.sendMessageToServer({
      text: this.formControls.messageText.value,
      authorName: this.socketService.userName,
      authorId: this.socketService.mySocketId,
      time: new Date()
    });

    this.formControls.messageText.setValue('');
    this.formGroup.updateValueAndValidity();

    return false; // to stop processing the event (to avoid adding empty line in textarea)
  }
}
