import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-room-header',
  templateUrl: './room-header.component.html',
  styleUrls: ['./room-header.component.scss']
})
export class RoomHeaderComponent {
  @Input() roomName: string;
  @Input() creatorName: string;
  @Output() leaveRoom: EventEmitter<void> = new EventEmitter<void>();
  @Output() copyLink: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('linkButton', { read: ElementRef }) linkButton: ElementRef;

  constructor(private _focusMonitor: FocusMonitor) {}

  onLeaveRoom(): void {
    this.leaveRoom.emit();
  }

  onCopyLink(): void {
    this.copyLink.emit();

    this._focusMonitor.stopMonitoring(this.linkButton.nativeElement);
  }
}
