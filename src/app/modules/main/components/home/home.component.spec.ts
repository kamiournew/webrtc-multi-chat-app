import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { click } from 'src/app/modules/utils/test-utils';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HomeComponent, SelectedTab } from './home.component';

class MockActivatedRoute {
  snapshot = {
    queryParams: {
      roomToken: 'test_token'
    }
  };
}

describe('HomeComponent', () => {
  let fixture: ComponentFixture<HomeComponent>;
  let component: HomeComponent;
  let el: DebugElement;
  let route: any;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [NoopAnimationsModule, MaterialModule],
        declarations: [HomeComponent],
        providers: [{ provide: ActivatedRoute, useClass: MockActivatedRoute }],
        schemas: [NO_ERRORS_SCHEMA]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(HomeComponent);
          component = fixture.componentInstance;
          el = fixture.debugElement;
          route = TestBed.inject(ActivatedRoute);
        });
    })
  );

  it('should create home component', () => {
    expect(component).toBeTruthy();
  });

  it('should set selected tab to "Join", when token is present in route', () => {
    component.ngOnInit();
    expect(component.selectedIndex).toEqual(SelectedTab.Join);
  });

  it('should set selected tab to "Create", when token is not present in route', () => {
    delete route.snapshot.queryParams.roomToken;
    component.ngOnInit();
    expect(component.selectedIndex).toEqual(SelectedTab.Create);
  });

  it('should have "Join" tab visible to user, when token is present in route', () => {
    component.ngOnInit();

    fixture.detectChanges();

    const activeTabTitle = el.query(By.css('.mat-tab-label-active .mat-tab-label-content')).nativeElement.textContent;

    expect(activeTabTitle).toEqual('Join a room');
  });

  it('should have "Create" tab visible to user, when token is not present in route', () => {
    delete route.snapshot.queryParams.roomToken;
    component.ngOnInit();
    fixture.detectChanges();

    const activeTabTitle = el.query(By.css('.mat-tab-label-active .mat-tab-label-content')).nativeElement.textContent;

    expect(activeTabTitle).toEqual('Create a room');
  });

  it('should switch to "Create" tab when corresponding tab label clicked', fakeAsync(() => {
    component.ngOnInit();
    fixture.detectChanges();

    const activeTabTitleBeforeClick = el.query(By.css('.mat-tab-label-active .mat-tab-label-content')).nativeElement
      .textContent;
    expect(activeTabTitleBeforeClick).toEqual('Join a room');

    const tabs = el.queryAll(By.css('.mat-tab-label'));
    click(tabs[0]);
    fixture.detectChanges();
    flush();

    const activeTabTitleAfterClick = el.query(By.css('.mat-tab-label-active .mat-tab-label-content')).nativeElement
      .textContent;
    expect(activeTabTitleAfterClick).toEqual('Create a room');
  }));
});
