import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export enum SelectedTab {
  Create = 0,
  Join
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  selectedIndex: SelectedTab = SelectedTab.Create;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    const roomToken = this.route.snapshot.queryParams.roomToken;

    if (!!roomToken) {
      this.selectedIndex = SelectedTab.Join;
    }
  }
}
