import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Message } from '../../components/video-room/chat/chat.component';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private _messages: Array<Message> = [];
  messageReceived: Subject<void> = new Subject<void>();

  get messages(): Array<Message> {
    return this._messages;
  }

  onTextMessageReceived(message: Message): void {
    this._messages.push(message);
    this.messageReceived.next();
  }

  setMessages(messages: Array<Message>): void {
    this._messages = messages;
  }

  clearMessages(): void {
    this._messages = [];
  }
}
