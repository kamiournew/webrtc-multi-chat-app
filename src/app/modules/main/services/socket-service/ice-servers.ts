export interface IceServers {
  iceServers: Array<{ urls: string | Array<string>; username?: string; credential?: string }>;
}

export const iceServers: IceServers = {
  iceServers: [
    {
      urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302']
    },
    {
      urls: ['turn:74.125.247.128:3478?transport=udp', 'turn:74.125.247.128:3478?transport=tcp'],
      username: 'CPvnhIIGEgZULRH113kYqvGggqMKIICjBTAK',
      credential: 'Et39yP1R4T11GostLlItvYPysGQ='
    }
  ]
};
