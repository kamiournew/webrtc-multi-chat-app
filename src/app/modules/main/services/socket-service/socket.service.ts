import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { io } from 'socket.io-client';
import { Socket } from 'socket.io-client/build/socket';
import { environment } from 'src/environments/environment';
import { Message } from '../../components/video-room/chat/chat.component';
import { ChatService } from '../chat-service/chat.service';
import { iceServers, IceServers } from './ice-servers';
import { UserMediaService } from '../user-media-service/user-media.service';

const SOCKET_ENDPOINT = environment.socketEndpoint;

export interface RoomCreateConfig {
  roomName: string;
  userName: string;
  maxNumber: number;
}

export interface RoomJoinConfig {
  roomToken: string;
  userName: string;
  muteAudio: boolean;
  hideVideo: boolean;
}

export interface ServerRoomConfig {
  name: string;
  createdBy: string;
  maxNumberOfGuests: number;
  chatHistory: Array<Message>;
}

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket: Socket;
  private _isRoomCreated: boolean;
  private _isFirstConnection = true;
  myRTCPeerConnections: { [key: string]: RTCPeerConnection } = {};
  myPeers: { [key: string]: { name: string; isInitialVideoHidden: boolean; isInitialAudioMuted: boolean } } = {};
  iceServers: IceServers = iceServers;
  mySocketId: string;
  currentRemoteSocketId: string;
  roomName: string;
  roomToken: string;
  userName: string;
  creatorName: string;
  maxNumberOfGuests: number;

  constructor(
    private readonly router: Router,
    private readonly toastr: ToastrService,
    private readonly userMediaService: UserMediaService,
    private readonly chatService: ChatService
  ) {}

  get isRoomCreated(): boolean {
    return this._isRoomCreated;
  }

  get isFirstConnection(): boolean {
    return this._isFirstConnection;
  }

  get linkToJoinRoom(): string {
    return `${window.location.origin}/home?roomToken=${this.roomToken}`;
  }

  initSocket(): void {
    this.socket = io(SOCKET_ENDPOINT);

    this.socket.on('connect_error', this.onConnectError.bind(this));
    this.socket.on('connected', this.onConnected.bind(this));
    this.socket.on('created', this.onCreated.bind(this));
    this.socket.on('joined', this.onJoined.bind(this));
    this.socket.on('full', this.onFull.bind(this));
    this.socket.on('ready', this.onReady.bind(this));
    this.socket.on('candidate', this.onCandidate.bind(this));
    this.socket.on('offer', this.onOffer.bind(this));
    this.socket.on('answer', this.onAnswer.bind(this));
    this.socket.on('leave', this.onLeave.bind(this));
    this.socket.on('roomAlreadyCreated', this.onRoomAlreadyCreated.bind(this));
    this.socket.on('noSuchRoom', this.onNoSuchRoom.bind(this));
    this.socket.on('chat-message', this.onTextMessage.bind(this));
    this.socket.on('user-video-toggled', this.userVideoToggled.bind(this));
    this.socket.on('user-audio-toggled', this.userAudioToggled.bind(this));
  }

  onCreateRoomButtonClicked(roomCreateConfig: RoomCreateConfig): void {
    this.roomName = roomCreateConfig.roomName;
    this.userName = roomCreateConfig.userName;
    this.creatorName = roomCreateConfig.userName;
    this.maxNumberOfGuests = roomCreateConfig.maxNumber;

    console.log('ROOM_NAME: ', this.roomName, this.userName);
    this.socket.emit('create-to-server', { ...roomCreateConfig, roomToken: this.mySocketId });
  }

  onJoinRoomButtonClicked(roomJoinConfig: RoomJoinConfig): void {
    ({ roomToken: this.roomToken, userName: this.userName } = roomJoinConfig);

    this.userMediaService.setHideVideoFlag(roomJoinConfig.hideVideo);
    this.userMediaService.setMuteAudioFlag(roomJoinConfig.muteAudio);

    console.log(this.roomToken);
    this.socket.emit('join-to-server', roomJoinConfig);
  }

  onLeaveRoomButtonClicked(): void {
    this.socket.emit('leave-to-server', this.roomToken, this.mySocketId);

    Object.keys(this.myRTCPeerConnections).forEach((key: string) => {
      this.clearMyRTCPeerConnections(key);
    });
    this.myPeers = {};

    this.chatService.clearMessages();
    this.destroySocketSubscriptions();
    this.initSocket();
  }

  onConnectError(error: Error): void {
    this.toastr.error('Could not establish connection with server', error.name);
  }

  onConnected(socketId: string): void {
    this.mySocketId = socketId;
    this.roomToken = socketId;

    if (this.isFirstConnection) {
      this.toastr.success('', 'Connection with server established');
    }

    this._isFirstConnection = false;

    console.log('CONNECTED: MY_SOCKET_ID: ', this.mySocketId);
  }

  onCreated(): void {
    console.log('I_CREATED_A_ROOM');
    this.toastr.success('', 'Room created');
    this._isRoomCreated = true;

    this.userMediaService
      .handleUserMedia()
      .pipe(
        tap(() => {
          this.toastr.success('', 'Your video and audio streams received');
          this.router.navigate(['', 'video-room', this.roomToken], { queryParams: { isCreate: true } });
        }),
        catchError((error: Error) => {
          this.socket.emit('leave-to-server', this.roomToken, this.mySocketId);
          this.toastr.error(error.message, 'Error getting user media');
          return of(error);
        })
      )
      .subscribe();
  }

  onJoined(roomConfig: ServerRoomConfig): void {
    console.log('I_JOINED_A_ROOM');
    this.toastr.success('', 'Room joined');

    this.roomName = roomConfig.name;
    this.creatorName = roomConfig.createdBy;
    this.maxNumberOfGuests = roomConfig.maxNumberOfGuests;
    this._isRoomCreated = true;
    this.chatService.setMessages(roomConfig.chatHistory);

    this.userMediaService
      .handleUserMedia()
      .pipe(
        tap(() => {
          this.toastr.success('', 'Your video and audio streams received');
          this.router.navigate(['', 'video-room', this.roomToken], { queryParams: { isCreate: false } });
        }),
        catchError((error: Error) => {
          this.socket.emit('leave-to-server', this.roomToken, this.mySocketId);
          this.toastr.error(error.message, 'Error getting user media');
          return of(error);
        })
      )
      .subscribe();
  }

  onFull(): void {
    this.toastr.error('', 'Room is full');
  }

  onRoomAlreadyCreated(): void {
    this.toastr.error('', 'Room has already been created');
  }

  onNoSuchRoom(): void {
    this.toastr.error('', 'No such room to join');
  }

  sendMessageToServer(message: Message): void {
    this.socket.emit('text-to-server', message, this.mySocketId, this.roomToken);
  }

  onTextMessage(message: Message, fromId: string): void {
    if (fromId !== this.mySocketId) {
      this.toastr.info(message.text, `Message from ${message.authorName}:`, {});
    }

    const messageFromServer: Message = {
      ...message,
      isMine: fromId === this.mySocketId
    };

    this.chatService.onTextMessageReceived(messageFromServer);
  }

  emitReady(): void {
    console.log(this.userMediaService.hideVideoFlag);

    this.socket.emit(
      'ready-to-server',
      this.roomToken,
      this.mySocketId,
      this.userName,
      this.userMediaService.hideVideoFlag,
      this.userMediaService.muteAudioFlag
    );
  }

  onReady(fromId: string, userName: string, isVideoHidden: boolean, isAudioMuted: boolean): void {
    if (fromId === this.mySocketId) {
      return;
    }
    console.log('READY_RECEIVED', this.myRTCPeerConnections, fromId, userName, isVideoHidden, isAudioMuted);

    this.setRTCPeerConnection(fromId);
    this.myPeers[fromId] = { name: userName, isInitialVideoHidden: isVideoHidden, isInitialAudioMuted: isAudioMuted };

    this.myRTCPeerConnections[fromId].createOffer().then(
      (offer: RTCSessionDescriptionInit) => {
        this.myRTCPeerConnections[fromId].setLocalDescription(offer);

        this.socket.emit(
          'offer-to-server',
          offer,
          this.roomToken,
          this.mySocketId,
          fromId,
          this.userName,
          this.userMediaService.hideVideoFlag,
          this.userMediaService.muteAudioFlag
        );

        this.toastr.info('', 'Sending offer');
      },
      (err: Error) => {
        this.toastr.error('', 'Error creating offer');
        console.log(err);
      }
    );
  }

  onCandidate(candidate: RTCIceCandidateInit, fromId: string, toId: string): void {
    if (this.mySocketId !== toId) {
      return;
    }
    console.log('CANDIDATE_RECEIVED', candidate, this.mySocketId, this.myRTCPeerConnections, fromId, toId);
    this.toastr.success('', 'Candidate received');

    const iceCandidate = new RTCIceCandidate(candidate);

    this.myRTCPeerConnections[fromId].addIceCandidate(iceCandidate);
  }

  onOffer(
    offer: RTCSessionDescriptionInit,
    fromId: string,
    toId: string,
    userName: string,
    isVideoHidden: boolean,
    isAudioMuted: boolean
  ): void {
    if (toId !== this.mySocketId) {
      return;
    }
    console.log('OFFER_RECEIVED', this.myRTCPeerConnections, fromId, toId);
    this.toastr.success('', 'Offer received');

    this.setRTCPeerConnection(fromId);

    this.myPeers[fromId] = {
      name: userName,
      isInitialAudioMuted: isAudioMuted,
      isInitialVideoHidden: isVideoHidden
    };

    this.myRTCPeerConnections[fromId].setRemoteDescription(offer);

    this.myRTCPeerConnections[fromId].createAnswer().then(
      (answer: RTCSessionDescriptionInit) => {
        this.myRTCPeerConnections[fromId].setLocalDescription(answer);
        this.socket.emit('answer-to-server', answer, this.roomToken, this.mySocketId, fromId);

        this.toastr.info('', 'Sending answer');
      },
      (err: Error) => {
        this.toastr.error('', 'Error creating answer');
        console.log(err);
      }
    );
  }

  onAnswer(answer: RTCSessionDescriptionInit, fromId: string, toId: string): void {
    if (toId !== this.mySocketId) {
      return;
    }

    console.log('ANSWER_RECEIVED', this.myRTCPeerConnections, fromId);
    this.toastr.success('', 'Answer received');

    this.myRTCPeerConnections[fromId].setRemoteDescription(answer);
  }

  onLeave(fromId: string): void {
    if (fromId === this.mySocketId) {
      return;
    }

    console.log('LEAVE_REQUEST_RECEIVED', this.myRTCPeerConnections, fromId);
    this.toastr.info('', 'Leave request received');

    this.clearMyRTCPeerConnections(fromId);
    delete this.myPeers[fromId];
    this.userMediaService.onPeerVideoRemoved(fromId);
  }

  onMuteAudioButtonClicked(isMuted: boolean): void {
    this.socket.emit('user-audio-toggled-to-server', this.roomToken, this.mySocketId, !isMuted);
  }

  onHideVideoButtonClicked(isVideoHidden: boolean): void {
    this.socket.emit('user-video-toggled-to-server', this.roomToken, this.mySocketId, !isVideoHidden);
  }

  userVideoToggled(fromSocketId: string, isEnabled: boolean): void {
    if (fromSocketId === this.mySocketId) {
      this.userMediaService.onHideVideoButtonClicked(!isEnabled);
      return;
    }

    this.userMediaService.onPeerVideoToggled(fromSocketId, this.myPeers[fromSocketId].name, !isEnabled);
  }

  userAudioToggled(fromSocketId: string, isEnabled: boolean): void {
    if (fromSocketId === this.mySocketId) {
      this.userMediaService.onMuteAudioButtonClicked(!isEnabled);
      return;
    }

    this.userMediaService.onPeerAudioToggled(fromSocketId, this.myPeers[fromSocketId].name, !isEnabled);
  }

  private setRTCPeerConnection(remoteSocketId: string): void {
    this.currentRemoteSocketId = remoteSocketId;

    if (!this.myRTCPeerConnections[remoteSocketId]) {
      this.myRTCPeerConnections[remoteSocketId] = new RTCPeerConnection(iceServers);
    }

    this.myRTCPeerConnections[remoteSocketId].onicecandidate = this.onICECandidateHandler.bind(this);

    // receiving and handling incoming video and audio streams from peer
    this.myRTCPeerConnections[remoteSocketId].ontrack = this.onTrack.bind(this);

    this.myRTCPeerConnections[remoteSocketId].oniceconnectionstatechange = () => {
      console.log('ICE state: ', this.myRTCPeerConnections[remoteSocketId].iceConnectionState);
    };

    this.myRTCPeerConnections[remoteSocketId].onicecandidateerror = event => {
      console.log('!ICE CANDIDATE ERROR: ', event.errorCode, event.url, event.errorText);
    };

    // sending our audio to peer
    this.myRTCPeerConnections[remoteSocketId].addTrack(
      this.userMediaService.userStream.getTracks()[0],
      this.userMediaService.userStream
    );

    // sending our video to peer
    this.myRTCPeerConnections[remoteSocketId].addTrack(
      this.userMediaService.userStream.getTracks()[1],
      this.userMediaService.userStream
    );
  }

  private onTrack(event: RTCTrackEvent): any {
    console.log('onTrack: ', this.currentRemoteSocketId);

    this.userMediaService.onPeerVideoReceived(
      event.streams[0],
      this.currentRemoteSocketId,
      this.myPeers[this.currentRemoteSocketId].name,
      this.myPeers[this.currentRemoteSocketId].isInitialVideoHidden,
      this.myPeers[this.currentRemoteSocketId].isInitialAudioMuted
    );
  }

  private onICECandidateHandler(event: RTCPeerConnectionIceEvent): void {
    if (!event.candidate) {
      return;
    }

    console.log('candidate handler', event.candidate);
    this.toastr.info('', 'Sending candidate');

    this.socket.emit(
      'candidate-to-server',
      event.candidate,
      this.roomToken,
      this.mySocketId,
      this.currentRemoteSocketId
    );
  }

  private clearMyRTCPeerConnections(connectionKey: string): void {
    console.log(this, connectionKey, this.myRTCPeerConnections);
    this.myRTCPeerConnections[connectionKey].ontrack = null;
    this.myRTCPeerConnections[connectionKey].onicecandidate = null;
    this.myRTCPeerConnections[connectionKey].close();
    delete this.myRTCPeerConnections[connectionKey];
  }

  private destroySocketSubscriptions(): void {
    this.socket.off('connected', this.onConnected.bind(this));
    this.socket.off('created', this.onCreated.bind(this));
    this.socket.off('joined', this.onJoined.bind(this));
    this.socket.off('full', this.onFull.bind(this));
    this.socket.off('ready', this.onReady.bind(this));
    this.socket.off('candidate', this.onCandidate.bind(this));
    this.socket.off('offer', this.onOffer.bind(this));
    this.socket.off('answer', this.onAnswer.bind(this));
    this.socket.off('leave', this.onLeave.bind(this));
    this.socket.off('roomAlreadyCreated', this.onRoomAlreadyCreated.bind(this));
    this.socket.off('noSuchRoom', this.onNoSuchRoom.bind(this));
  }
}
