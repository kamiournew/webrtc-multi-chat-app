import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { from, Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

export interface PeerVideoConfig {
  id: string;
  srcObject: MediaStream;
  isAudioMuted: boolean;
  isVideoHidden: boolean;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserMediaService {
  private _userStream: MediaStream;
  private _hideVideoFlag = false;
  private _muteAudioFlag = false;

  private _peerVideosCollection: Array<PeerVideoConfig> = [];

  peerVideoReceived = new Subject<PeerVideoConfig>();
  peerVideoRemoved = new Subject<string>();

  constructor(private readonly toastr: ToastrService) {}

  get userStream(): MediaStream {
    return this._userStream;
  }

  get hideVideoFlag(): boolean {
    return this._hideVideoFlag;
  }

  get muteAudioFlag(): boolean {
    return this._muteAudioFlag;
  }

  get peerVideosCollection(): Array<PeerVideoConfig> {
    return this._peerVideosCollection;
  }

  handleUserMedia(): Observable<MediaStream> {
    this.toastr.info('', 'Trying to get your video and audio streams');

    // 1280 x 960 - higher quality video config
    return from(navigator.mediaDevices.getUserMedia({ audio: true, video: { width: 320, height: 240 } })).pipe(
      tap((mediaStream: MediaStream) => {
        this.setUserStream(mediaStream, this.muteAudioFlag, this.hideVideoFlag);
      })
    );
  }

  setUserStream(mediaStream: MediaStream, shouldMuteAudio: boolean, shouldHideVideo: boolean): void {
    this._userStream = mediaStream;
    this._userStream.getTracks()[0].enabled = !shouldMuteAudio;
    this._userStream.getTracks()[1].enabled = !shouldHideVideo;
  }

  onMuteAudioButtonClicked(isMuted: boolean): void {
    this.setMuteAudioFlag(isMuted);
    this._userStream.getTracks()[0].enabled = !isMuted;
  }

  onHideVideoButtonClicked(isVideoHidden: boolean): void {
    this.setHideVideoFlag(isVideoHidden);
    this._userStream.getTracks()[1].enabled = !isVideoHidden;
  }

  onPeerVideoReceived(
    stream: MediaStream,
    remoteSocketId: string,
    peerName: string,
    isVideoHidden: boolean,
    isAudioMuted: boolean
  ): void {
    console.log('Media Stream received: ', stream);
    this.toastr.success('', 'Peer video received');

    const videoId = 'peer-video_' + remoteSocketId;

    const videoCollectionHasSuchElem = this._peerVideosCollection.findIndex(item => item.id === videoId) !== -1;

    if (!videoCollectionHasSuchElem) {
      this._peerVideosCollection.push({
        id: videoId,
        srcObject: stream,
        isAudioMuted,
        isVideoHidden,
        name: peerName
      });
    }
  }

  onPeerVideoRemoved(remoteSocketId: string): void {
    this.toastr.info('', 'Peer disconnected');

    const videoId = 'peer-video_' + remoteSocketId;

    this._peerVideosCollection = this._peerVideosCollection.filter(peerVideoObj => peerVideoObj.id !== videoId);
  }

  onPeerVideoToggled(remoteSocketId: string, peerName: string, isVideoHidden: boolean): void {
    console.log('Peer video toggled: ', remoteSocketId, isVideoHidden);
    this.toastr.info('', `${peerName} ${isVideoHidden ? 'disabled' : 'enabled'} video`);

    const videoId = 'peer-video_' + remoteSocketId;

    const videoElem = this._peerVideosCollection.find(item => item.id === videoId);

    videoElem.isVideoHidden = isVideoHidden;
    console.log(videoElem);
  }

  onPeerAudioToggled(remoteSocketId: string, peerName: string, isAudioMuted: boolean): void {
    console.log('Peer audio toggled: ', remoteSocketId, isAudioMuted);
    this.toastr.info('', `${peerName} ${isAudioMuted ? 'disabled' : 'enabled'} mic`);

    const videoId = 'peer-video_' + remoteSocketId;

    const videoElem = this._peerVideosCollection.find(item => item.id === videoId);

    videoElem.isAudioMuted = isAudioMuted;
  }

  setHideVideoFlag(shouldHide: boolean): void {
    this._hideVideoFlag = shouldHide;
  }

  setMuteAudioFlag(shouldMute: boolean): void {
    this._muteAudioFlag = shouldMute;
  }
}
