import { AfterContentInit, Component, ContentChild, ElementRef, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
  selector: 'app-video-room-layout',
  templateUrl: './video-room-layout.component.html',
  styleUrls: ['./video-room-layout.component.scss']
})
export class VideoRoomLayoutComponent implements AfterContentInit {
  @ContentChild('drawerToggle', { read: ElementRef }) drawerToggleButton: ElementRef;
  @ViewChild('drawer') drawer: MatDrawer;

  ngAfterContentInit(): void {
    this.drawerToggleButton.nativeElement.addEventListener('click', () => {
      this.drawer.toggle();
    });
  }
}
