import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoRoomLayoutComponent } from './video-room-layout.component';

xdescribe('VideoRoomLayoutComponent', () => {
  let component: VideoRoomLayoutComponent;
  let fixture: ComponentFixture<VideoRoomLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VideoRoomLayoutComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoRoomLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
