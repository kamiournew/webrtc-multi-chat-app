import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateRoomFormComponent } from './components/create-room-form/create-room-form.component';
import { JoinRoomFormComponent } from './components/join-room-form/join-room-form.component';
import { AppFormControlsModule } from 'src/app/shared/components/form-controls/form-controls.module';
import { VideoRoomComponent } from './components/video-room/video-room.component';
import { VideoRoomLayoutComponent } from './layouts/video-room-layout/video-room-layout.component';
import { ChatComponent } from './components/video-room/chat/chat.component';
import { ChatLayoutComponent } from './layouts/chat-layout/chat-layout.component';
import { RoomHeaderComponent } from './components/video-room/room-header/room-header.component';
import { ChatMessageComponent } from './components/video-room/chat-message/chat-message.component';

@NgModule({
  declarations: [
    HomeComponent,
    CreateRoomFormComponent,
    JoinRoomFormComponent,
    VideoRoomComponent,
    VideoRoomLayoutComponent,
    ChatComponent,
    ChatLayoutComponent,
    RoomHeaderComponent,
    ChatMessageComponent
  ],
  imports: [CommonModule, MainRoutingModule, SharedModule, AppFormControlsModule]
})
export class MainModule {}
