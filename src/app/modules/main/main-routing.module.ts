import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from 'src/app/shared/components/not-found-page/not-found-page.component';
import { HomeComponent } from './components/home/home.component';
import { VideoRoomComponent } from './components/video-room/video-room.component';
import { VideoRoomGuard } from './guards/video-room.guard';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'video-room',
    component: VideoRoomComponent,
    children: [
      {
        path: ':roomToken',
        canActivate: [VideoRoomGuard],
        runGuardsAndResolvers: 'always',
        component: VideoRoomComponent
      }
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: '**',
    redirectTo: '/404'
  },
  {
    path: '404',
    component: NotFoundPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
