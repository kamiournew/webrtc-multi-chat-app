import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SocketService } from './modules/main/services/socket-service/socket.service';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let el: DebugElement;
  let socketService: any;

  beforeEach(
    waitForAsync(() => {
      const socketServiceSpy = jasmine.createSpyObj('SocketService', ['initSocket']);

      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [AppComponent],
        providers: [{ provide: SocketService, useValue: socketServiceSpy }]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(AppComponent);
          component = fixture.componentInstance;
          el = fixture.debugElement;
          socketService = TestBed.inject(SocketService);
        });
    })
  );

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title of "webRTC-multi-chat"', () => {
    expect(component.title).toEqual('webRTC-multi-chat');
  });

  it('should have call initSocket method once', () => {
    component.ngOnInit();
    expect(socketService.initSocket).toHaveBeenCalledTimes(1);
  });
});
