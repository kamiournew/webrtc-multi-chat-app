import { Component, OnInit } from '@angular/core';
import { SocketService } from './modules/main/services/socket-service/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'webRTC-multi-chat';

  constructor(private socketService: SocketService) {}

  ngOnInit(): void {
    this.socketService.initSocket();
  }
}
