# Defining the default image for each jobs
# Using same node version as I have locally
image: node:10.19.0

# Defining stages and their sequence
stages:
  - build
  - test
  - deploy review
  - deploy develop
  - deploy staging
  - deploy production
  - production tests
  - cache

# Defining cache stage which is scheduled to run daily
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
  policy: pull

# Defining main variables
variables:
  DEVELOP_DOMAIN: develop-webrtc-multi-chat-pet-project.surge.sh
  STAGING_DOMAIN: staging-webrtc-multi-chat-pet-project.surge.sh
  PRODUCTION_DOMAIN: webrtc-multi-chat-pet-project.surge.sh

# Job for building the website once the code goes to master or develop branch
# or on merge requests
# Keeping ./dist folder as an artifact for future jobs
build website:
  stage: build
  only:
    - master
    - develop
    - merge_requests
  except:
    - schedules
  script:
    - npm install
    - npm install -g @angular/cli
    - npm run build-surge
  artifacts:
    paths:
      - ./dist

# Job for testing the build using project unit tests written with Jasmine
# It is also run on master, develop and merge requests
# An image with Chrome installed is used for this job
test build:
  image: atlassianlabs/docker-node-jdk-chrome-firefox
  stage: test
  only:
    - master
    - develop
    - merge_requests
  except:
    - schedules
  cache: {}
  script:
    - npm install
    - npm install -g @angular/cli
    - npm run test-ci

# Job for testing whether the website is deployed successfully
# Grepping a phrase from the index.html file
test website:
  stage: test
  only:
    - master
    - develop
    - merge_requests
  except:
    - schedules
  script:
    - npm install
    - npm install -g @angular/cli
    - npm run start &
    - sleep 120
    - curl "http://localhost:4200" | tac | tac | grep -q "WebRTCMultiChat"

# Job for deploying changes which are presented every PR
# Useful to compare a prod or stage version with the uploaded version
deploy review:
  stage: deploy review
  only:
    - merge_requests
  except:
    - schedules
  cache: {}
  script:
    - echo https://$CI_ENVIRONMENT_SLUG.surge.sh
    - npm install --global surge
    - surge --project ./dist --domain https://$CI_ENVIRONMENT_SLUG.surge.sh
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.surge.sh
    on_stop: stop review

# A job for removing the website deployed by the job above (deploy review)
# Runs right after a pull request is merged (changes approved)
stop review:
  stage: deploy review
  only:
    - merge_requests
  variables:
    GIT_STRATEGY: none
  script:
    - npm install --global surge
    - surge teardown $BASE_NAME-$CI_ENVIRONMENT_SLUG.surge.sh
  when: manual
  cache: {}
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop

# A template job used as a base for deploy jobs
.deploy_template: &deploy
  cache: {}
  except:
    - schedules
  script:
    - npm install --global surge
    - surge --project ./dist --domain https://$DOMAIN
  environment:
    url: https://$DOMAIN

# 3 jobs below are for deploying prod, staging and develop versions of the app
# Prod deploy is done manually only
deploy develop:
  <<: *deploy
  only:
    - develop
  stage: deploy develop
  variables:
    DOMAIN: $DEVELOP_DOMAIN
  environment:
    name: develop

deploy staging:
  <<: *deploy
  only:
    - master
  stage: deploy staging
  variables:
    DOMAIN: $STAGING_DOMAIN
  environment:
    name: staging

deploy production:
  <<: *deploy
  only:
    - master
  when: manual
  allow_failure: false
  stage: deploy production
  variables:
    DOMAIN: $PRODUCTION_DOMAIN
  environment:
    name: production

# A job for testing a deployed prod version of the app
production tests:
  stage: production tests
  image: alpine
  only:
    - master
  except:
    - schedules
  cache: {}
  script:
    - apk add --no-cache curl
    - curl -s "https://$PRODUCTION_DOMAIN" | grep -q "WebRTCMultiChat"

# A scheduled job for caching node_modules
# The schedule itself is set up in GitLab account
update cache:
  stage: cache
  only:
    - schedules
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
    policy: push
  script:
    - npm install
